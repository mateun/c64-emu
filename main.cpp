#include <iostream>
#include <Windows.h>
#include <vector>



void* bitmapMemory;
BITMAPINFO bitmapInfo;
int mouseX, mouseY = 0;



struct Regs {
    uint8_t x = 0;
    uint8_t y = 0;
    uint8_t acc = 0;
    uint16_t pc = 0;
    uint8_t instr = 0;
};

struct Flags {
    bool N;
    bool V;
    bool E;
    bool B;
    bool D;
    bool I;
    bool Z;
    bool C;
};

struct cpu6510 {
    Regs regs;
    Flags flags;
    
};

struct system_c64 {
    uint8_t memory[0xFFFF];
    uint8_t dataBus = 0;
    uint16_t addressBus = 0;
    cpu6510 cpu;
    uint32_t totalCycles = 0;
    bool sync = false;
    int instr_cycle = 0;
};

void init_system(system_c64& system) {
    system.addressBus = system.cpu.regs.pc;
    system.dataBus = system.memory[system.addressBus];
    system.cpu.regs.instr = system.dataBus;
    system.sync = true;
}

void fetch(system_c64& system) {
    system.addressBus = system.cpu.regs.pc;
    system.dataBus = system.memory[system.addressBus];
    system.cpu.regs.instr = system.dataBus;
}

void doCycle(system_c64& system) {

    if (system.sync) {
        fetch(system);
    }

    switch (system.cpu.regs.instr) {
        // LDA imm
    case 0xa9: {
        if (system.instr_cycle == 0) {
            system.cpu.regs.pc++;
            system.addressBus = system.cpu.regs.pc;
            system.sync = false;
            system.instr_cycle++;
            break;
        }

        if (system.instr_cycle == 1) {
            system.dataBus = system.memory[system.addressBus];
            system.cpu.regs.acc = system.dataBus;
            system.cpu.regs.pc++;
            system.instr_cycle = 0;
            system.sync = true;
            break;
        }
        }
    }

    system.totalCycles++;
    
}


void dump_sys_info(HDC hdc, system_c64& sys) {
    char buffy[100];
    ZeroMemory(buffy, 100);
    
    
    RECT rt;
    rt.left = 5;
    rt.right = 200;
    rt.top = 450;
    rt.bottom = 475;
    sprintf_s(buffy, 40, "N%d V%d E%d B%d D%d I%d Z%d C%d", sys.cpu.flags.N, sys.cpu.flags.V, 
            sys.cpu.flags.E, sys.cpu.flags.B, sys.cpu.flags.D, 
            sys.cpu.flags.I, sys.cpu.flags.Z, sys.cpu.flags.C );
    DrawText(hdc, TEXT(buffy),
        -1, &rt, 0);

    rt.left = 5;
    rt.right = 80;
    rt.top = 425;
    rt.bottom = 445;
    sprintf_s(buffy, 30, "Cycle: %u", sys.totalCycles);
    DrawText(hdc, TEXT(buffy),
        -1, &rt, 0);

    rt.left = 85;
    rt.right = 140;
    rt.top = 425;
    rt.bottom = 445;
    sprintf_s(buffy, 30, "PC: %#02x", sys.cpu.regs.pc);
    DrawText(hdc, TEXT(buffy),
        -1, &rt, 0);

    rt.left = 145;
    rt.right = 290;
    rt.top = 425;
    rt.bottom = 445;
    ZeroMemory(buffy, 100);
    sprintf_s(buffy, 60, "Databus: %#0x ", sys.dataBus);
    DrawText(hdc, TEXT(buffy),
        -1, &rt, 0);


    rt.left = 295;
    rt.right = 540;
    rt.top = 425;
    rt.bottom = 445;
    ZeroMemory(buffy, 100);
    sprintf_s(buffy, 40, "Addr.Bus: %#02x", sys.addressBus);
    DrawText(hdc, TEXT(buffy),
        -1, &rt, 0);


    rt.left = 550;
    rt.right = 750;
    rt.top = 425;
    rt.bottom = 445;
    ZeroMemory(buffy, 100);
    sprintf_s(buffy, 90, "A:%#0x X:%#0x Y:%#0x ", sys.cpu.regs.acc, sys.cpu.regs.x, sys.cpu.regs.y);
    DrawText(hdc, TEXT(buffy),
        -1, &rt, 0);



}

system_c64 c64;




LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    HDC hdc;
    RECT rect;
    TEXTMETRIC tm;
    PAINTSTRUCT ps;

    switch (message) {

        case WM_LBUTTONUP: {
            int xPos = LOWORD(lParam);
            int yPos = HIWORD(lParam);
            mouseX = xPos;
            mouseY = yPos;
            
        }
        break;

        case WM_KEYDOWN:
            switch (wParam)
            {
            case VK_SPACE:
                doCycle(c64);
                RedrawWindow(hwnd, NULL, NULL, RDW_INVALIDATE | RDW_UPDATENOW);
                break;

            case VK_RIGHT:
                
                break;

            case VK_ADD:
                
                break;
            case VK_SUBTRACT:
                
                break;

            case VK_NUMPAD4:
                
                break;

            case VK_NUMPAD6:
                
                break;

            case VK_RETURN:
                
                break;
            }
            RedrawWindow(hwnd, NULL, NULL, RDW_INVALIDATE | RDW_UPDATENOW);
            break;

        case WM_CLOSE:
           /* if (MessageBox(NULL, "Are you sure you want to quit?",
                           "Confirmation", MB_ICONQUESTION | MB_YESNO) == IDYES)*/
            DestroyWindow(hwnd);
            break;

        case WM_DESTROY:
            PostQuitMessage(0);
            break;

        case WM_PAINT: {

            hdc = BeginPaint(hwnd, &ps);
           
            GetClientRect(hwnd, &rect);
            DrawText(hdc, TEXT ("Screen: 320x200"),
                     -1, &rect, DT_SINGLELINE | DT_LEFT | DT_TOP);
            
            
            RECT btnRect;
            btnRect.left = 710;
            btnRect.top = 20;
            btnRect.bottom = 40;
            btnRect.right = 730;

            HBRUSH brush = CreateSolidBrush(RGB(250, 15, 30));
            FillRect(hdc, &btnRect, brush) ;
            DeleteObject(brush);

            brush = CreateSolidBrush(RGB(50, 151, 151));
            SelectObject(hdc, brush);

            POINT p1;
            p1.x = 680;
            p1.y = 20;
            POINT p2;
            p2.x = 700;
            p2.y = 30;
            POINT p3;
            p3.x = 680;
            p3.y = 40;
            
            POINT pts[] = { p1, p2, p3 };
            
            Polygon(hdc, pts, 3);
           
            DeleteObject(brush);

            GetClientRect(hwnd, &rect);
           
            dump_sys_info(hdc, c64);

            EndPaint(hwnd, &ps);

            break;
        }
        default:
            return DefWindowProc(hwnd, message, wParam, lParam);
    }
    return 0;
}

void createBackbuffer(int w, int h, void** backBuffer) {

    if (bitmapMemory) {
        VirtualFree(nullptr, 0, MEM_RELEASE);
    }

    bitmapInfo.bmiHeader.biSize = sizeof(bitmapInfo.bmiHeader);
    bitmapInfo.bmiHeader.biWidth = w;
    bitmapInfo.bmiHeader.biHeight = h;
    bitmapInfo.bmiHeader.biPlanes = 1;
    bitmapInfo.bmiHeader.biBitCount = 32;
    bitmapInfo.bmiHeader.biCompression = BI_RGB;

    int bytesPerPixel = 4;
    int bitMemorySize = (w * h) * bytesPerPixel;
    *backBuffer = VirtualAlloc(nullptr, bitMemorySize, MEM_COMMIT, PAGE_READWRITE);

}

void setPixel(int x, int y, float r, float g, float b, void* backBuffer) {
    UINT32* bb = (UINT32*)backBuffer;
    // TODO replace hardcoded 320 by actual screen width
    int offset = (y * 320) + x;
    int pixel = 0;
    pixel = (int)(r * 255) << 16;
    pixel |= (int)(g * 255) << 8;
    pixel |= (int)(b * 255);

    bb[offset] = pixel;
}

void swapBackbuffer(HDC windowDC, int x, int y, int w, int h, void* backBuffer) {
    StretchDIBits(windowDC,
        x, y, w, h,
        // TODO replace hardcoded values
        0, 0, 320, 200,
        backBuffer,
        &bitmapInfo,
        DIB_RGB_COLORS,
        SRCCOPY
    );

}

float pos = 0;

uint8_t chars[] = {

    // A
    0, 0, 0, 1, 1, 0, 0,0,
    0, 0, 1, 1, 1, 1, 0,0,
    0, 1, 1, 0, 0, 1, 1,0,
    0, 1, 1, 0, 0, 1, 1,0,
    0, 1, 1, 1, 1, 1, 1,0,
    0, 1, 1, 1, 1, 1, 1,0,
    0, 1, 1, 0, 0, 1, 1,0,
    0, 1, 1, 0, 0, 1, 1,0,

    // B
    1, 1, 1, 1, 1, 1, 1,0,
    1, 1, 1, 1, 1, 1, 1,1,
    1, 1, 1, 0, 0, 1, 1,1,
    1, 1, 1, 0, 0, 1, 1,0,
    1, 1, 1, 1, 1, 1, 1,0,
    1, 1, 1, 0, 0, 1, 1,1,
    1, 1, 1, 0, 0, 1, 1,1,
    1, 1, 1, 1, 1, 1, 1,0,

    // C
    1, 1, 1, 1, 1, 1, 1,1,
    1, 1, 1, 1, 1, 1, 1,1,
    1, 1, 1, 0, 0, 0, 1,0,
    1, 1, 1, 0, 0, 0, 0,0,
    1, 1, 1, 0, 0, 0, 0,0,
    1, 1, 1, 0, 0, 0, 1,0,
    1, 1, 1, 0, 0, 1, 1,1,
    1, 1, 1, 1, 1, 1, 1,1,

};

void drawChar(int x, int y, void* backBuffer, char c) {

    int offset = 0;
    switch (c) {
    case 'b': offset = 64; break;
    case 'c': offset = 128; break;
    };
    
    for (int cx = 0; cx < 8; cx++) {
        for (int cy = 0; cy < 8; cy++) {
            uint8_t index = cx + cy * 8;
            uint8_t val = chars[index + offset];
            if (val) {
                setPixel(x + cx, (y - cy), 1, 1, 1, bitmapMemory);
            }
        }
        
    }
}


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
            LPSTR lpCmdLine, int nCmdShow)
{
    const char g_szClassName[] = "bonafideideasWindowClass";
    WNDCLASSEX wc;

    wc.lpszMenuName  = NULL;
    wc.hInstance     = hInstance;
    wc.lpszClassName = g_szClassName;
    wc.cbSize        = sizeof(WNDCLASSEX);
    wc.cbClsExtra      = wc.cbWndExtra  = 0;
    wc.style         = CS_HREDRAW | CS_VREDRAW ;
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);
    wc.lpfnWndProc = WndProc;

    if(!RegisterClassEx(&wc))    {
        MessageBox(NULL, "Window Registration Failed", "Error",   MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    HWND hwnd = CreateWindowEx(
            WS_EX_CLIENTEDGE,
            g_szClassName,
            "C64-emulator",
            WS_OVERLAPPEDWINDOW,
            CW_USEDEFAULT, CW_USEDEFAULT, 800, 600,
            NULL, NULL, hInstance, NULL);

    if(hwnd == NULL)    {
        MessageBox(NULL, "Window Creation Failed", "Error",   MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    ShowWindow(hwnd, nCmdShow);
    UpdateWindow(hwnd);

    
    createBackbuffer(320, 200, &bitmapMemory);

    c64.memory[0] = 0xa9;
    c64.memory[1] = 0xAB;
    c64.memory[2] = 0xa9;
    c64.memory[3] = 0xCC;
    init_system(c64);

    HDC hdc = GetDC(hwnd);
    MSG msg;
    while (true) {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
            if (msg.message == WM_QUIT) break;
         
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }

        // cls
        ZeroMemory(bitmapMemory, 320 * 200 * 4);

        pos += 0.03f;
        for (int i = 0; i < 5; i++) {
            setPixel((int)pos + i, 10, 1, 0.2, 0.1, bitmapMemory);
            setPixel((int)pos + i, 11, 1, 0.2, 0.1, bitmapMemory);
            setPixel((int)pos + i, 12, 1, 0.2, 0.1, bitmapMemory);
            setPixel((int)pos + i, 13, 1, 0.2, 0.1, bitmapMemory);
            setPixel((int)pos + i, 14, 1, 0.2, 0.1, bitmapMemory);


            setPixel(320 - (int)pos + i, 110, .1, 0.2, 0.9, bitmapMemory);
            setPixel(320 - (int)pos + i, 111, .1, 0.2, 0.9, bitmapMemory);
            setPixel(320 - (int)pos + i, 112, .1, 0.2, 0.9, bitmapMemory);
            setPixel(320 - (int)pos + i, 113, .1, 0.2, 0.9, bitmapMemory);
            setPixel(320 - (int)pos + i, 114, .1, 0.2, 0.9, bitmapMemory);
        }
        for (int i = 0; i < 320; i++) {
            setPixel(i, 180, sin(i), i, i, bitmapMemory);
            setPixel(i, 181, cos(i), i, i, bitmapMemory);
            setPixel(i, 182, sin(i), i, i, bitmapMemory);
            setPixel(i, 183, cos(i), i, i, bitmapMemory);
            setPixel(i, 184, sin(i), i, i, bitmapMemory);


            
        }

        drawChar(pos*0.1, 50, bitmapMemory, 'a');
        drawChar(80, 50, bitmapMemory, 'b');
        drawChar(200-pos*0.123, 50, bitmapMemory, 'c');
        drawChar(200, 50, bitmapMemory, 'b');
        
        swapBackbuffer(hdc, 5, 20, 640, 400, bitmapMemory);
    }
   

    return (int) msg.wParam;
}



